package com.privacera.application;

import com.privacera.application.dao.EmployeeDao;
import com.privacera.application.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application  {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}