package com.privacera.application.controller;

import com.privacera.application.dao.EmployeeDao;
import com.privacera.application.exception.ExceptionCaught;
import com.privacera.application.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {
    @Autowired
    private EmployeeDao employeeDao;

    @GetMapping(value = "employee/getAllEmployee")
    public List<Employee> getAllEmployee(){
        return employeeDao.findAll();
    }

    @GetMapping(value = "employee/{eid}")
    private Employee getEmployeeById(@PathVariable Long eid){
        return employeeDao.findById(eid).orElseThrow(()-> new ExceptionCaught(eid.toString()));
    }

    @PostMapping(value = "employee/add")
    private Employee addEmployee(@RequestBody Employee e){
        return employeeDao.save(e);
    }

    @PutMapping(value = "employee/update/{eid}")
    private ResponseEntity<Employee> updateEmployye(@PathVariable Long eid, @RequestBody Employee emp){
        Employee updateEmployee = employeeDao.findById(eid).orElseThrow(()->new ExceptionCaught("Employee not exits with id "+eid));
        updateEmployee.setEname(emp.getEname());
        updateEmployee.setEsalary((long)emp.getEsalary());

        employeeDao.save(updateEmployee);
        return ResponseEntity.ok(updateEmployee);
    }

    @DeleteMapping(value = "employee/delete/{eid}")
    private ResponseEntity<HttpStatus> deleteEmployee(@PathVariable Long eid){
        Employee e = employeeDao.findById(eid).orElseThrow(()-> new ExceptionCaught("mployee not exits with id "+eid));
        employeeDao.delete(e);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
