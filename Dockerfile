FROM hub74.privacera.us/privacera_debian_base_jdk17:10.27.0.1-1
ARG PKG_NAME_TGZ=springboot.tar.gz
ARG API_SERVER_FOLDER=springboot

RUN mkdir /workdir \
  && mkdir /workdir/${API_SERVER_FOLDER}

COPY ./target/${PKG_NAME_TGZ} /workdir/${API_SERVER_FOLDER}

RUN cd /workdir/${API_SERVER_FOLDER} \
  && ls -l \
  && tar xfz ./${PKG_NAME_TGZ} \
  && rm ${PKG_NAME_TGZ}

WORKDIR /workdir/${API_SERVER_FOLDER}

ENV PRIVACERA_HOME=/workdir/${API_SERVER_FOLDER}

ENTRYPOINT ["bin/entrypoint.sh"]